import { Router, Request, Response } from 'express';
import { isEmail, isPassword, isUsername, isName } from '../config/validation';
import { User } from '../schemas/user';
import { signToken } from '../config/validation';
import "dotenv/config";

const router: Router = Router();

router.post('/', async function (req: Request, res: Response) {
    const { name, username, email, password } = req.body;

    // Check if email and password are valid with regex
    if (!isEmail(email)) {
        return res.status(400).json({ error: 'Invalid email!' });
    }

    if (!isPassword(password)) {
        return res.status(400).json({ error: 'Password must be at least 8 characters long and it must contain at least 1 number!' });
    }

    if (!isUsername(username)) {
        return res.status(400).json({ error: 'Invalid username!' });
    }

    if (!isName(name)) {
        return res.status(400).json({ error: 'Invalid name format!' });
    }

    // Check if username exists
    const foundUser = await User.findOne({ username });
    if (foundUser) {
        return res.status(400).json({ error: 'Username is already taken' });
    }

    // Check if email exists
    const foundEmail = await User.findOne({ email });
    if (foundEmail) {
        return res.status(400).json({ error: 'Email is already taken' });
    }

    // If everything is okay
    // Create a new user
    const newUser = new User({ name, username, email, password });
    await newUser.save(function (err, data) {
        if (err) throw err;
    });

    // Return token of user
    const token = signToken(req.user);
    res.status(200).json({ token: token });
});

export const RegisterController: Router = router;