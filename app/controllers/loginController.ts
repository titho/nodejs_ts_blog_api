import { Router } from 'express';
import { signToken } from '../config/validation';
import { isPassword, isUsername } from '../config/validation';
import 'dotenv/config';

const router: Router = Router();

router.post('/', function (req, res) {
    
    if (!isPassword(req.body.password)) {
        return res.status(400).json({ error: 'Invalid password!' });
    }

    if (!isUsername(req.body.username)) {
        return res.status(400).json({ error: 'Invalid username!' });
    }

    // Generate token
    const token = signToken(req.user);
    res.status(200).json({ token });
});

//export login controller
export const LoginController: Router = router;