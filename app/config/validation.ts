import jwt from 'jsonwebtoken';
import { IUserModel } from '../schemas/user';
import "dotenv/config"

export let signToken = (user: IUserModel) => {
    return jwt.sign({
        iss: 'Blog',
        sub: user.id,
        iat: new Date().getTime(), //current time
        exp: new Date().setDate(new Date().getDate() + 1) //current time + 1 day
    }, String(process.env.JWT_SECRET));
}

export function isEmail(email: String): boolean {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export function isPassword(password: String) {
    const re = new RegExp("^(?=.*[a-z])(?=.*[0-9])(?=.{6,})");
    return re.test(String(password));
}

export function isUsername(username: string) {
    const re = /^(?=.{3,30}$)[a-z0-9_.-\s]+$/i;
    return re.test(String(username));
}

export function isName(name: string) {
    const re = /^(?=.{3,30}$)[a-z\s]+$/i;
    return re.test(String(name));
}